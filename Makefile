###
# Some convenience commands to build and install the package
###
PACKAGE:=gengli
INSTALL_DIR:=$(shell python -c "import numpy; print(numpy.__file__.replace('numpy/__init__.py', 'gengli'))")

all:
	@echo Please, give me a target!
	@echo ${INSTALL_DIR}

install: package
	cd .. && pip install $(PACKAGE)/dist/$(PACKAGE)-0.1.0.tar.gz && cd $(PACKAGE)

build: docs package
	@echo All built

docs: gengli/* docs/*
	sphinx-build -b html docs/ docs/__build/
	
package:
	python setup.py sdist

install_cp:
	@if [ -d $(INSTALL_DIR) ]; then \
		cp gengli/*.py $(INSTALL_DIR)/; \
		cp gengli/ctgan/*.py $(INSTALL_DIR)/ctgan/; \
		echo Copied files into \"$(INSTALL_DIR)\"; \
	else \
		echo Installation directory \"$(INSTALL_DIR)\" does not exist: unable to install the files.; \
		echo Have you installed the package normally?; \
	fi
	
#
#cp gengli/ctgan/*.py $(INSTALL_DIR)/ctgan/

clean:
	rm -rf docs/__build gengli.egg-info/ dist/ gengli.egg-info/ gengli/__pycache__ gengli/ctgan/__pycache__
