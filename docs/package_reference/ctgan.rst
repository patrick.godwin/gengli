``gengli.ctgan``
----------------

.. automodule:: gengli.ctgan

.. automodule:: gengli.ctgan.model

.. autoclass::  gengli.ctgan.model.GenerativeNet
	:members:

.. autoclass::  gengli.ctgan.model.DiscriminativeNet
	:members:

.. automodule:: gengli.ctgan.train
	:members:
