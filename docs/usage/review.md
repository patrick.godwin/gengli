Review
======

The code underwent a review by the LIGO/Virgo/Kagra collaboration. The reviewers were ...  and ...

The purpose of the review is to ...

Below we document the review steps.

The review was signed off on version `0.2.0`.

Review environment
------------------

All the review was performed in a conda environment `gengli_review_env`, specified by the file [`review_env.yml`](https://git.ligo.org/melissa.lopez/gengli/-/blob/main/review_env.yml). The enviroment makes sure that all the tests are performed on a standardized set of pacakge dependencies. Moreover, as the dependencies are updated, some compatibility isses may arise (although this is very rare); the `gengli_review_env` provides an environment where the code is _guaranteed_ to work.

Note that this is a different environment from the `gengli_env` defined by `env.yml`, which gets the lates compatible version of each dependency.

To get the `gengli_review_env` you can execute:

```Bash
conda env create --file review_env.yml
```
