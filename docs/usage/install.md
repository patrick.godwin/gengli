Installation
============

The easiest way to install the package and its dependencies is with `pip`.
Several options are available

## From the latest distributed version

You can get the latest distributed version from the [PyPI](https://pypi.org/project/gengli/) archives.
To do this, you just need to type:

```
pip install gengli
```

This will install the package and its dependencies in your current working python environment.

## From source

If you want to get the latest developments, you can install the package from source.
To do this you need to clone the git [repository](https://git.ligo.org/melissa.lopez/gengli/-/tree/main) and to build and install the code manually. A handy makefile will help you to do that.

These are the steps:


```Bash
git clone git@git.ligo.org:melissa.lopez/gengli.git
cd gengli
make install
```

This will build the package and will install it: `pip` will keep track of the dependencies.

If you want to build your own distribution, you can do as follows:

```Bash
git clone git@git.ligo.org:melissa.lopez/gengli.git
cd gengli
python setup.py sdist
pip install dist/gengli-0.0.1.tar.gz
```

## With conda

If you want to create a fresh conda environment with `gengli` and its dependencies, the file `env.yml` is what you're looking for:

```Bash
conda env create --file env.yml
```
(How to force pip to install gengli also if the file lives in the same folder as `gengli/`?)

## Build the docs

To get a local copy of this documentation:

```Bash
cd gengli
python setup.py build_sphinx
```

The documentation will be generated on `gengli/docs/__build`.



