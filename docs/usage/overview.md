Overview
========

`gengli` is a handy package to generate loud random transient noise artifacts, a.k.a. _glitches_, in gravitational waves (GW) instruments. Glitches can be as frequent as 1 per minute and are a common hindrance to GW detection: their study and modelling is crucial to improve the quality of GW data analysis.
This package moves a step in that direction employing a [Generative Adversarial Neural Network](https://papers.nips.cc/paper/2014/hash/5ca3e9b122f61f8f06494c97b1afccf3-Abstract.html) with a consistency term (CT-GAN) to model a training set of random glitches.

`gengli` offers an easy to use interface to load the weights of two trained networks and to evaluate their predictions. The two networks are fitted using _blip_ glitches observed during the second observing run O2 of the LIGO/Virgo collabortation.
One network reproduces glitches from Hanford observatory (H1) while the other glitches from the Livingston observatory (L1).

The networks output a _whitened_ glitch evluated on a custom grid of `938` points at a sampling rate of `4096 Hz`. This is what we call 'raw glitch'.
`gengli` offers a number of handy post-precessing, which may be useful for GW data analysis: it can resample the glitch to an user given sampling rate and it can scale it to an user given signal-to-noise ratio (SNR).

On top of such standard preprocessing, `gengli` can generate glitches with a given "degree of anomaly". See next [section](validation.md) for more information on this.

To know more about the CT-GAN model to generate glitches, you can read the accompaying [paper](https://arxiv.org/abs/2203.06494), which describes the methods, validate the network and outlines some future applications.
Another [paper](https://arxiv.org/abs/2205.09204), describes in more details the network architecture and the current package.

Below you can find more information about glitches and a quick (but rather extensive) guide on how to generate glitches and processing them using `gengli`. Enjoy!

What is a glitch?
-----------------

A glitch is how we call informally a _transient bursts of non-Gaussian noise_ in the detectors. Indeed, the appearance of a glitch breaks the stationariety (and the gaussianity) of the noise of a detector with a short and intense signal-like transients. Glitches have durations typically on the order of sub-seconds, and their causes can be environmental (e.g., earthquakes, wind, anthropogenic noise) or instrumental (e.g., overflows, scattered light), although in many cases, the cause remains unknown.
As they are loud and similar to signals, they remain one of the major limiting factors in the detection and parameter estimation of transient GW signals, especially those with short duration.

Glitches came in different classes, as classified by the Machine Learning (ML) based software [Gravity Spy](https://arxiv.org/abs/1611.04596). They are classified for their different shapes and morphology and there is an active field of research to detect them and perform a proper classification.
The most common class is called "blip" but other, less common, classes have been detected such as "Whistle", "Chirp" and "Koi Fish".

Basic Usage
-----------

The core of `gengli` is implemented in the class `gengli.glitch_generator`, which loads the weights of the network and provides an interface to all the features described above. To import `gengli`, you shoul simply type:

```Python
import gengli
```

As discussed above, the package comes with two pretrained models, for H1 and L1 instruments.
If you want to load one of this two pretrained models you should give a string `'L1'` or `'H1'` at the class initialization. If you want to try your own model, you should provide a valid path to a `torch` weigth files for the generator network (argument `weight_file`). In this case, it is up to you to ensure that the file is consistent with the network. You should be safe if you generate the glitch within the `gengli` framework: see [here](training.md) for more information.

To instantiate a generator with the defaults `'H1'` weigths, you should type:

```Python
g = gengli.glitch_generator('H1')
```

You are now ready to generate a glitch:

```Python
g.get_glitch()
```

This will store in a numpy array a single whithened glitch with a sampling rate of 4096, evaluated on a standard grid of 938 points. An option `glitch_type` specifies the type of glitch to generate: unfortunately, only `Blip` type is currently supported but we hope this to be extended in future. You can also create glitches in batches (argument `n_glitches`): this will speed up the generation time!
Moreover, as the glitch generation is a random process, you can specify a `seed` for reproducible results.

A typical glitch will look like this:

![raw_glitch](../img/raw_glitch.png)

### Lots of manipulations

In a typical situation, you may want to perform some post-processing to the glitch, to make it ready for injection in real/simulated data. No worries! We got it covered: the function `get_glitch()` already does what you're looking for it allows to:
- scale the glitch to a given SNR (option `snr`)
- resample the glitch to the given sample rate (option `srate`)
- window the glitch with a Tukey window, with a given alpha parameter (option `alpha`)
- filter out the high frequencies (option `fhigh`). This amounts to applying a low pass filter to the glitch: this usually removes some high frequency disturbances and makes the glitch smoother.
- select them according to a given confidence interval (argument `confidence_interval`). This should be a tuple `(percentile_min, percentile_max)`. The function will return only glitches whose anomaly scores are included in the given confidence range for _all_ the three measures described above. If you set this option, the first call of the function will take a while, since an heavy initialization step will be performed. You can do the initialization before by calling `initialize_benchmark_set` with the appropriate sampling rate.

If you don't want to perform one of the operation above, you can just set the relevant argument to `None`. The transformation won't be applied.

For example, if you want to generate 10 windowed glitches @ `2048 Hz` with SNR 15 and with the high frequency content removed, you can just type this:

```Python
g.get_glitch(10,
	srate = 2048,
	snr = 15,
	alpha = 0.2,
	fhigh = 250)
```

After all the manipulations, the glitch above (`seed = 0`) will look like this:

![glitch](../img/glitch.png)

The class `glitch_generator` provides also a number of helpers functions. For instance, `get_len_glitch` will tell you the size of the time grid for the glitch at a given sampling rate. `get_fft_grid` will provide the frequency grid at which `np.fft.fft` will evaluate the given glitch (of course it is dependent on the sampling rate).
If you want to load again the weigths for the network, you should call `load_generator_weights`.

### The `gengli.utils` module

The module `gengli.utils` keeps some utilities that are used in the main class.
`gengli.utils.compute_distance_matrix` computes the three metrics between two sets of glitches. The metric computation is implemented as a ray function in `gengli.utils.metrics`.

### To know more

As usual, to know more information about the low level behavior of the functions you can use the built-in help functions:

```Python
help(gengli.glitch_generator)
```














