Training information
====================

Training dataset
-----------------

In this study, we input "Blip" glitches classified with [Gravity Spy](https://arxiv.org/abs/1611.04596). In our work, we access (non-public) [`Glitch DB`](https://ldvw.ligo.caltech.edu/ldvw/gspySearch) and download CSV files containing the information regarding "Blip" glitches with a `ml_confindence` $\geq 0.9$ for Livingston (L1) and Hanford (H1). Note that this information is private, but non-LVK researchers can get the GPS time of "Blip" glitches for Livingston and Hanford on Zenodo website [here](https://zenodo.org/record/5649212#.YrHBzC0lNQL). 

After selecting these "Blip" glitches we perform the procedure explained in our [paper](https://arxiv.org/abs/2205.09204) employing [BayesWave](https://arxiv.org/abs/1410.3835) and [rROF](https://arxiv.org/pdf/1409.7888.pdf). 

The first step to run BayesWave is to install it. For this purpose we create our own conda environment:

`conda create -n bayeswave_env python=3.8`

We access our environment `conda activate bayeswave_env` and follow BayesWave installation procedure, that can be found [here](https://docs.ligo.org/lscsoft/bayeswave/install.html#). Once it is installed, we can source the environment of BayesWave, as described by the installation output. In my case it is as follows:

`source /home/melissa.lopez/opt/lscsoft/bayeswave/etc/bayeswave-user-env.sh`

To test if BayesWave is working properly, we can run `bayeswave_pipe --help`. Note that it is possible that we need to install certain packages like `numpy`, `glue.lw` and `lalsuite` beforehand. We can install them as follows (provided you have python3 installed):

```bash
pip install numpy
pip install lscsoft-glue
pip install lalsuite
```

Once we have sourced BayesWave environment, we can set its workflow to run a condor job with the following command:

```bash
bayeswave_pipe /path/to/config.ini \
    --trigger-list /path/to/trigger_times.txt \
    --workdir /path/to/working_dir \
    --bayesline-median-psd \
    --skip-megapy \
```

We specify `--bayesline-median-psd` that computes many psds and the median, which is then used for BayesWave run. We use it to avoid reproducibility issues. We also specify `--skip-megapy` to skip plotting results and speed up the analysis.

Note that while we provide `gengli/examples/example_config.ini` as an example, it is necessary to extract glitch trigger times from (non-public)[`Glitch DB`](https://ldvw.ligo.caltech.edu/ldvw/gspySearch) or [Zenodo](https://zenodo.org/record/5649212#.YrHBzC0lNQL). 

Furthermore, we provide an example sh file, namely `gengli/examples/example_gen_job.sh`, to set up the workflow automaticaly.

Training procedure
-----------------

All the code related to the ML model is accessible under the name `gengli.ctgan`. The networks architectures (i.e. the generator and the discriminator) are defined in `gengli.ctgan.model`. The code for training them is wrapped in function `training_loop` of module `gengli.ctgan.train`.

Luckily, the training of the GAN network can be performed using a handy [script](https://git.ligo.org/melissa.lopez/gengli/-/blob/main/examples/train_ctgan.py) with one single command line.
The script will make the relevant initializations and will call the training loop.

The minimal command to run the training script is:

```Bash
python train_ctgan.py --training-set dummy_dataset.dat --out-dir gengli-dummy-model
```

The dataset must be given after all the suitable preprocessing and must be a file holding a `np.array` where each rows is a different glitch evaluated on a standard time grid. For instance, this snippet will create a fake dataset out of the network generated glitches:

```Python
import gengli
import numpy as np
g = gengli.glitch_generator('L1')
glitch_list = [ g.get_raw_glitch(100) for _ in range(10)]
np.savetxt('dummy_dataset.dat', np.concatenate(glitch_list, axis = 0))
```

Training options
----------------

Although the script looks simple enough, a bunch of parameters control the behavior of the training and you can change them freely. However, we do not recommend it: they are difficult to validate and you can safely rely on the defaults. You can add the following parameters to the previous line:

- `--batch-size` : Number of training samples seen by the network for every forward pass (default=32)
- `--n-epochs` : Number of epochs, number of times every training sample will be seen (default=500)
- `--lr-D` : Learning rate for the Discriminator (default=0.0001)
- `--lr-G` : Learning rate for the Generator (default=0.0001)
- `--ifo` : Detector from which training samples will be used (default = 'H1', choices= ['H1', 'L1'])
<space>

- `--ncycles-D` : Number of iterations of the Discriminator per iteration of the Generator (default=5)
- `--dropout-rate` : Dropout rate for the discriminator (default=0.6)
- `--length-noise` : Length of the input random vector (default = 100)
- `--num-channel-D` : Starting number of convolution filters for the Discriminator (default = 64)
- `--num-channel-G` : Starting number of convolution filters for the Generator (default = 64)
- `--lambda-gp` : Loss weight for the gradient penalty (default=5.0)
- `--lambda-ct` : Loss weight for the consistency term (default=5.0)
<space>

The second part of the parameters is direclty related to the network itself and the loss used to train it. The lamda parameters allow the network to converge by weighting each term in the loss of the Discriminator : 

<img src="https://latex.codecogs.com/gif.latex?L_{tot} = L_{WGAN} + \lambda_{gp}\,GP + \lambda_{ct}\,CT" /> 

For more details about the loss and the training method, see [our paper](https://arxiv.org/abs/2203.06494). 


