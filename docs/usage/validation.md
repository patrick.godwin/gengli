# Further examples

In the [previous](overview.md) section, we provided an overview of the capabilities of the code and we called a million times the function `gengli.glitch_generator.get_glitch()`.
We also show how to load the PSD from a file (and from a stream of bytes!)

In this section, you can find some examples of how to use more advanced features of the code.

## Compute the SNR

Let's check that the SNR of the scaled glitch is the one that we actually injected!

First we generate a glitch and we scale it to SNR 15.

```Python
import gengli

srate = 4096.
g = gengli.glitch_generator('L1')
glitch = g.get_glitch(1, snr = 15, srate = srate)
```

Now we use `pycbc` to generate 10 seconds of gaussian white noise and we inject the glitch 3 seconds the start.

```Python
from pycbc.noise import reproduceable

white_noise = reproduceable.normal(0, 10, sample_rate = srate)

white_noise[int(3*srate):int(3*srate)+len(glitch)] += glitch
```

We now filter the data with the injected glitch as a template. The resulting SNR timeseries should peak of almost 15 around `T=3`.

```Python
import pycbc.filter
import numpy as np

glitch_template = np.concatenate([glitch, np.zeros(len(white_noise)-len(glitch))])
glitch_template = pycbc.types.timeseries.TimeSeries(glitch_template,
	delta_t = 1./srate, dtype = np.float64)
snr_timeseries = pycbc.filter.matchedfilter.matched_filter(glitch_template, white_noise, psd=None)
```

The resulting time series looks like that: not bad, right?

![q transform](../img/snr_timeseries.png)


## Generating glitches in a confidence interval

As with any stochastic sampling, the glitches will be all different from each other.
Of course, some of them will be outliers of the sampling distribution (whatever that is), whereas others will be "normal" ones.
Depending on the application, you could imagine filtering glitches based on their "anomaly score".

The anomaly score is computed starting from 3 measures of distances between glitches:

- [Wasserstein distance](https://en.wikipedia.org/wiki/Wasserstein_metric)
- Mis-[Cross correlation](https://en.wikipedia.org/wiki/Cross-correlation): this is computed as 1-cross correlation
- [Mismatch](https://sbozzolo.github.io/kuibit/gw_mismatch.html#overlap-and-mismatch): this is a standard measure of distances between GW signals, commonly used in GW data analysis.

Given these 3 distances, one can compute the distance matrix (for each of the measures) between two sets of glitches:

```
import gengli
from gengli.utils import compute_distance_matrix, metrics

g = gengli.glitch_generator('L1')

set1 = g.get_glitch(20)
set2 = g.get_glitch(10)

distance_matrix = compute_distance_matrix(set1, set2)
distance = metrics(set1[0], set2[0])
```

In this case, the distance matrix has shape `(20,10,3)` where the distances are in the order above.
By calling `metrics`, you can compute the distance between two glitches.

To measure the adcnomaly score, a _benchmark set_ with `N` points is initialized and the relative distances between all such points are computed according to the 3 measures: this is done by calling `compute_distance_matrix(benchmark_set, benchmark_set)`.
From the set of all the mutual distances, we can compute a histogram of relative distances between points in the benchmark set (populated with `N(N-1)/2` elements). This will create a benchmark against which the anomaly score will be computed.

For a newly generated glitch, we compute the 3 distances between that glitch and all the benchmark set and we take the mean (separately for each measure).
The anomaly score of a new glitch (for each measure) is the percentile of the average distance of the glitch for the histogram of distances between elements in the benchmark set, as we represent below.

![benchmark](../img/benchmark.png)

The user can then filter the glitches by their anomaly score by specifying a range in percentiles that the average distance of a glitch should lie in. Glitches will be drawn at random until they fit in the user-given percentile.
Glitches in the benchmark set are always _raw glitches_ (plus an eventual resampling): for this reason, each glitch must be compared with the benchmark before being recaled or undergoing any other manipulation.

This may sound a bit involved but the practice is very simple.
If you want to generate three glitches with a confidence interval in the range `[0,80]` (that's a rather normal glitch), you can type:

```Python
import gengli
g = gengli.glitch_generator('L1')
g.get_glitch_confidence_interval(percentiles = (0,80),
	n_glitches = 3)
```

The function `get_glitch_confidence_interval` accepts all the arguments you can provide to get glitch.

It first calls `initialize_benchmark_set` and it will automatically initialize the benchmark set with 100 glitches. The initialization takes a while as all the mutual distances between many glitches must be computed.

If you want to change the number of glitches in the benchmark set, you can initialize the benchmark set before generating the glitches

```Python
g.initialize_benchmark_set(200)
g.get_glitch_confidence_interval(percentiles = (90,100), n_glitches = 1)
```
This will produce a rather anomalous glitch (in a rather long time!)

## Injecting in real data and plotting a spectrogram

Once we have generated our glitch population we would like to inject them into detector noise. Firstly, let's generate a single glitch with SNR 30.

```Python
import gengli
import numpy as np

# Parameters
ifo = 'H1'
snr = 30
srate = 4096

g = gengli.glitch_generator(ifo)
glitch = g.get_glitch(snr = 30)
```

Now we load a strain of real data from H1. We want a strain of 20s, but since we need to whiten it we load other extra 20s to avoid border effects. Note that we also compute the PSD as we will use it later.

```Python
from gwpy.timeseries import TimeSeries
import pycbc

noise = TimeSeries.fetch_open_data(ifo, 1262540000, 1262540040, sample_rate=srate)
noise = noise.to_pycbc()
white_noise, psd = noise.whiten(len(noise) / (2 * srate),
                                len(noise)/( 4 * srate),
                                remove_corrupted = False,
                                return_psd = True)
```

The glitch is very short to be injected in our whitened strain of noise, so we need to padd zeros around it to add it to the background. Note that we want our glitch in the centre of the background.

```Python
length = noise.shape[-1]
len_glitch = glitch.shape[-1]
t_inj = 0.5 * length / srate

id_start = int( (t_inj * srate / length) * len(white_noise)) - len_glitch // 2
white_noise[id_start:id_start+len_glitch] += glitch
```
Once the glitch has been injected in our whitened we could re-colour it according to the original PSD. Afterwards, we crop 18 s at each side to avoid border effects.

```Python
colored_noise = (white_noise.to_frequencyseries() * psd ** 0.5).to_timeseries()
colored_noise = colored_noise[int(srate * 18):-int(srate * 18)]
```

Once the glitch has been injected into our `numpy` array, we might want to compute its Q-transform. We can easily do this with `gwpy`.

```Python
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter

data = TimeSeries(colored_noise, sample_rate = srate)
# Q-transform with Gravity Spy standards
q_scan = data.q_transform(qrange=[4,64], frange=[10, 2048],
                          tres=0.002, fres=0.5, whiten=True)

# We plot according to Gravity Spy standards
fig, ax  = plt.subplots(dpi=120)
ax.imshow(q_scan, cmap='viridis')
ax.set_yscale('log', base=2)
ax.set_xscale('linear')
ax.set_ylabel('Frequency (Hz)', fontsize=14)
ax.set_xlabel('Time (s)', labelpad=0.1,  fontsize=14)
ax.yaxis.set_major_formatter(ScalarFormatter())
ax.tick_params(axis ='both', which ='major', labelsize = 14)
cb = ax.colorbar(label='Normalized energy',clim=[0, 25.5])
ax.set_xlim(1.5, 2.5)
```

![q transform](../img/plot_q_transform.png)

## Classifying injections with Gravity Spy

Once we have injected a glitch, we might want to use Gravity Spy to obtain a class probability and a label for our input data. Note that the `event_time` of the glitch is zero since it is located in the centre of the time series.

```Python
from gravityspy.classify import classify

channel_name = 'H1:GDS-CALIB_STRAIN'
results_glitch = classify(event_time=0,
                          channel_name=channel_name,
                          path_to_cnn='./GravitySpy/models/sidd-cqg-paper-O3-model.h5',
                          timeseries=data)

glitch_label = results_glitch['ml_label'][0]
glitch_conf = results_glitch['ml_confidence'][0]
```
Here, `glitch_label` is the label provided by Gravity Spy, and `glitch_conf` is the probability assigned to this label.

This is what the 4 frames that Gravity Spy uses look like: 
![gravity spy](../img/gravspy.png)


