About
=====

Hi!
This project started as a master thesis of Kerwin Buijsman at Utrecht University, under the supervision of Sarah Caudill. With time, it grew into something larger and many other people joined the project:

- Vincent Boudart
- Melissa Lopez
- Amit Reza
- Stefano Schmidt

This work is accompained by a main papaer, describing the methods, and a proceeding paper, giving more details on the ML model and the accompayining software (i.e. `gengli`).
If you find it useful for your scientific work, please consider citing it as:

	@article{glitch_gan:2022,
  		doi = {10.48550/ARXIV.2203.06494},
		url = {https://arxiv.org/abs/2203.06494},
		author = {Lopez, Melissa and Boudart, Vincent and Buijsman, Kerwin and Reza, Amit and Caudill, Sarah},
		title = {Simulating Transient Noise Bursts in LIGO with Generative Adversarial Networks},
		publisher = {arXiv},
		year = {2022},
	}

The proceeding paper has the following entry:

	@article{Lopez:2022dho,
	author = "Lopez, Melissa and Boudart, Vincent and Schmidt, Stefano and Caudill, Sarah",
	title = "{Simulating Transient Noise Bursts in LIGO with gengli}",
	eprint = "2205.09204",
	archivePrefix = "arXiv",
	primaryClass = "astro-ph.IM",
	month = "5",
	year = "2022"
	} 
