import matplotlib.pyplot as plt
import gengli
import numpy as np

###########################
### Raw & processed glitches

g = gengli.glitch_generator('H1')
raw_glitch = g.get_glitch(seed = 0)
t_grid = np.linspace(0, len(raw_glitch)/4096., len(raw_glitch))

plt.figure()
plt.plot(t_grid, raw_glitch, c= 'orange')
plt.xlabel("t(s)")
plt.title('Raw glitch')
plt.savefig('raw_glitch.png')


processed_glitch = g.get_glitch(1,
	srate = 2048,
	snr = 15,
	alpha = 0.2,
	fhigh = 250,
	seed = 0)
	
t_grid = np.linspace(0, len(processed_glitch)/2048., len(processed_glitch))

plt.figure()
plt.plot(t_grid, processed_glitch, c= 'orange')
plt.xlabel("t(s)")
plt.title('Processed glitch')
plt.savefig('glitch.png')



###########################
### Matched filtering
from pycbc.noise import reproduceable
import pycbc.filter

srate = 4096.
g = gengli.glitch_generator('L1')
glitch = g.get_glitch(1, snr = 15, srate = srate)

white_noise = reproduceable.normal(0, 10, sample_rate = srate)

white_noise[int(3*srate):int(3*srate)+len(glitch)] += glitch

glitch_template = np.concatenate([glitch, np.zeros(len(white_noise)-len(glitch))])
glitch_template = pycbc.types.timeseries.TimeSeries(glitch_template,
	delta_t = 1./srate, dtype = np.float64)
snr_timeseries = pycbc.filter.matchedfilter.matched_filter(glitch_template, white_noise, psd=None)


plt.figure()
plt.title('SNR timeseries for a glitch SNR = 15 filtered by a glitch')
plt.plot(snr_timeseries.get_sample_times(), np.abs(snr_timeseries), c= 'blue')
plt.xlabel("t(s)")
plt.savefig('snr_timeseries.png')
plt.show()





















