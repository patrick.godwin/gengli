# Examples

Here you can find some examples on how to use `gengli` on some useful application. Have fun!

## plot_glitch.py

It creates a nice plot of a glitch.
This is an excuse for showing some of the functionalities of the code, such as resampling, rescaling, colouring and generating glitches with a given anomalous score.

If you download a [csv psd](https://dcc.ligo.org/LIGO-T2000012/public), you can have fun colouring the glitches with your favourite one.

## train_ctgan.py

With this you can train a new ctgan model. Unfortunately, we cannot share the original data as they are private. However, if you're interested to see how the training can work you can generate a tiny dataset of gengli Livingstone glitches and retrain a model.

To generate a simple dataset of 1000 glitches:

```Python
import gengli
import numpy as np
g = gengli.glitch_generator('L1')
glitch_list = [ g.get_raw_glitch(100) for _ in range(10)]
np.savetxt('dummy_dataset.dat', np.concatenate(glitch_list, axis = 0))
```
	
You are now ready to train a model. There is a huge bunch a hyperparameters but typically you don't want to touch them.
A minimal command to run would be:

```Bash
python train_ctgan.py --training-set dummy_dataset.dat --n-epochs 100 --batch-size 32 --out-dir gengli-dummy-model
```

This will store in the folder `gengli-dummy-model` the weigths of the trained model as well as the value of the loss functions. Be aware that the training is very expensive to perform on your laptop, without a GPU.

To know the available options you can type: `python train_ctgan.py --help` and take a look of what's there.

## glitch_bank.py

This is a test to place a template bank of glitches (this should be familiar to those of you who have dedicate some time to GW searches). The script keeps the code to perform a minimal stochastic placement using a rejection method to achieve a target minimum match.
There are not nice plots to show here but you can take a look at the code and see `gengli` in action in a relatively complicated code.

## example_config.ini

This script is an example of [BayesWave](https://docs.ligo.org/lscsoft/bayeswave/running.html) ini file to denoise blip glitches as performed in our research. 

## example_gen_job.sh

This sh file generates a condor job for `example_config.ini`. The pre-requisites to run this script is to follow the instructions in `gengli/docs/usage/training.md`.