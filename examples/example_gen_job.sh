#!bin/bash -e
eval "$(conda shell.bash hook)"
conda activate bayeswave_env

source /home/melissa.lopez/opt/lscsoft/bayeswave/etc/bayeswave-user-env.sh

config="./example_config.ini"
workdir="./test"
trigtimes="./times_O2.txt"

bayeswave_pipe ${config} \
    --trigger-list ${trigtimes} \
    --workdir ${workdir} \
    --bayesline-median-psd \
    --skip-megapy \
