"""
An example on how to inject the glitch into noise. The job is done by function `add_glitch`, which relies on pycbc.
The scripts makes use of gwpy to load and access gw data.
"""

import sys
sys.path.insert(0, '..')
import gengli
import numpy as np
import gwpy.timeseries
import matplotlib.pyplot as plt
import pycbc.filter, pycbc.types, pycbc.psd
from pycbc.noise.gaussian import noise_from_psd
from pycbc.types import TimeSeries, FrequencySeries
import scipy
import subprocess, os

###################################################################################################

def add_glitch(glitch, noise, T = None, t_inj = None, srate = 4096., white_output = False):
	"""
	Adds a glitch to some user-given noise timeseries. The glitch is *always* added to a whithened timeseries (i.e. where noise has the same variance at all frequencies).
	If ``white_output`` is true, a whithened time series will be returned, a coloured time series otherwise.
	The function also works in batches, allow to inject different glitches in (different) noise realisation(s).
	
	Parameters
	----------

	glitch: :class:`~numpy:numpy.ndarray`
		An array holding one or several glitches (every glitch is stored in a row of the array, see `glitch_generator.get_raw_glitch`)
	
	noise: :class:`~numpy:numpy.ndarray`
		A noisy time series (needs to have the same sampling rate as the glitch). If given, the glitch(es) will be injected in it
		`N` realizations of noise of length `D` can be given as a `(N,D)` array.
	
	T: float
		Length of the noisy time series. The given noise will be cropped accordingly. If T is None, all the given noise will be used
	
	t_inj: float
		Time at which the glitch should be injected. If None, the glitch will be injected in the middle of the segment

	white_output: float
		Whether to return a whitened timeseries (rather than a coloured one).
	
	Returns
	-------
		glitchy_timeseries: :class:`~numpy:numpy.ndarray`
			The timesieries with the added glitches.
	"""
	if isinstance(noise, (np.ndarray, list)): noise = np.array(noise)
	squeeze = (np.asarray(glitch).ndim == 1)
	glitch = np.atleast_2d(np.array(glitch))

	length = int(T*srate) if T else noise.shape[-1]
	df = srate/length
	len_glitch = glitch.shape[-1]
	if t_inj is None: t_inj = 0.5*length/srate
	
	assert t_inj< length/srate, ValueError("The noise timeseries is too short for the given time for the injection")
	
	if noise.ndim == 1:	noise = [TimeSeries(noise, delta_t = 1./srate) for _ in glitch]
	glitchy_timeseries = []
	
	for g, n in zip(glitch, noise):
	
		#Dealing with the noise
		len_zero = len(n)//2
		#noise_pycbc = pycbc_TimeSeries(np.concatenate([np.zeros(len_zero), n, np.zeros(len_zero)]), delta_t = 1./srate)
		noise_pycbc = TimeSeries(n[:length], delta_t = 1./srate)

		white_noise_pycbc, psd = noise_pycbc.whiten(2,1, remove_corrupted = False, return_psd = True)
		white_noise_pycbc *= scipy.signal.windows.tukey(len(white_noise_pycbc), alpha = 0.07)

		#Adding glitches
		id_start = int((t_inj*srate/length)*len(white_noise_pycbc)) - len_glitch//2
		
		white_noise_pycbc[id_start:id_start+len_glitch] += g*scipy.signal.windows.tukey(len(g), alpha = 0.1) #/np.sqrt(srate/2.)
		
		if white_output:
			out = white_noise_pycbc
		else:
				#Re-colouring
			out = (white_noise_pycbc.to_frequencyseries() * psd**0.5).to_timeseries()

		#Adding to the output array
		glitchy_timeseries.append(np.array(out))
		
	glitchy_timeseries = np.stack(glitchy_timeseries, axis = 0)

	if squeeze: return np.squeeze(glitchy_timeseries)
	return glitchy_timeseries


def plot_q_transform(data, srate = 4096., crop = None, whiten = True, ax = None, **kwargs):
	"""
	Plot the q tranform of a time series (it relies on gwpy for the q transform)

	Parameters
	----------
		data: gwpy TimeSeries
			Input data to plot
		dur: float
			Time window (in s) to compute Q transform
	"""
	# We need to resample the data to reach 2kHz
	data = gwpy.timeseries.TimeSeries(data, sample_rate = srate)
	srate = 16384
	#data = data.resample(srate)
	
	# Q-transform with Gravity Spy standards
	q_scan = data.q_transform(qrange=[4,64], 
							  frange=[10, 2048/2],
							  tres=0.002,
							  fres=0.5,
							  whiten=whiten,
							  **kwargs)

	if isinstance(crop, (list, tuple)):
		t_center, dur = crop
		t_center = t_center + data.t0.value
		q_scan = q_scan.crop(t_center-dur/2, t_center+dur/2)

		xticklabels = np.linspace(t_center-dur/2, t_center+dur/2, 5)
	
	# We plot according to Gravity Spy standards
	if ax is None: fig, ax  = plt.subplots(dpi=120)
	ax.imshow(q_scan)
	ax.set_yscale('log', base=2)
	ax.set_xscale('linear')
	if isinstance(crop, (list, tuple)):
		ax.set_xticks(xticklabels)
		ax.set_xticklabels(xticklabels)
	ax.set_ylabel('Frequency (Hz)', fontsize=14)
	ax.set_xlabel('Time (s)', labelpad=0.1,  fontsize=14)
	#ax.yaxis.set_major_formatter(ScalarFormatter())
	ax.tick_params(axis ='both', which ='major', labelsize = 14)
	cb = ax.colorbar(label='Normalized energy',clim=[0, 25.5])
	return


if __name__ == '__main__':
	
	user_snr = 25. #snr of the injection
	srate = 4096. #sampling rate
	T = 4 #Lenght of the stretch of data to consider

		#Reading the file
		#To get this file wget https://www.gw-openscience.org/eventapi/html/GWTC-3-confident/GW200322_091133/v1/H-H1_GWOSC_4KHZ_R1-1268903496-32.hdf5
	noise_filename = 'H-H1_GWOSC_4KHZ_R1-1268903496-32.hdf5'
	if not os.path.isfile(noise_filename):
		subprocess.run('wget https://www.gw-openscience.org/eventapi/html/GWTC-3-confident/GW200322_091133/v1/H-H1_GWOSC_4KHZ_R1-1268903496-32.hdf5', shell = True)
	noise_ts = gwpy.timeseries.TimeSeries.read(noise_filename, format = 'hdf5.gwosc')

	if noise_ts.sample_rate.value != srate: noise_ts.resample(srate)

		#######
		# Adding glitches to data
		
			
		#Adding a glitch to real noise
	g = gengli.glitch_generator('H1')
	glitch = g.get_glitch(1, snr = user_snr)
	
	glitch_ts = add_glitch(glitch, noise = noise_ts, T = T, t_inj = 2.5, srate = 4096., white_output = False)
	plot_q_transform(glitch_ts)
	plt.suptitle("Glitch added to real noise from file\n{}".format(noise_filename))
	
	plt.show()
	
	
	

	
	
	
	
	
	
