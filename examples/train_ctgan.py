"""
Handy script to train the GANN model. It initialise the networks and calls the training loop `gengli.ctgan.train`.
It takes as an input a path to the data.
The training hyperparameters as well as some architecture parameters are given as optional arguments. The user is strongly advised to leave the default values, unless they have a strong reason to do so.
"""

import torch
import numpy as np

import sys #DEBUG
sys.path.insert(0,'..') #DEBUG
from gengli.ctgan import model as model
from gengli.ctgan.train import training_loop

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--training-set',  type=str, required = True,
		help='Path to real input data')
parser.add_argument('--out-dir',  type=str, default = './gengli-trained-ctgan',
		help='Path to a folder to store the weigths and losses')
parser.add_argument('--batch-size', type=int, default = 32,
		help='Size of the batch for each training loop')
parser.add_argument('--n-epochs', type=int, default = 500,
		help='Number of training loops')
parser.add_argument('--lr-D', type=int, default = 0.0001,
		help='Learning rate for the discriminator')
parser.add_argument('--lr-G', type=int, default = 0.0001,
		help='Learning rate for the generator')
parser.add_argument('--ifo', type=str, default = 'H1', choices= ['H1', 'L1'],
		help='String for the interferometer to label the weights')

	#User typically doesn't want to touch the following options
parser.add_argument('--ncycles-D', type=int, default = 5,
		help='Number of iterations of the Discriminator per iteration of the Generator')
parser.add_argument('--lambda-gp', type=float, default = 5.,
		help='Loss weight for the gradient penalty')
parser.add_argument('--lambda-ct', type=float, default = 5.,
		help='Loss weight for the consistency term parameters')
parser.add_argument('--M-ct', type=float, default = 0.1,
		help='Constant parameter involved in the computation of the Consistency Term (CT). Recommended values are between 0 and 0.2')
parser.add_argument('--dropout-rate', type=float, default = 0.6,
		help='Dropout rate for the discriminator')
parser.add_argument('--length-noise', type=int, default = 100,
		help='Length of the input random vector')	
parser.add_argument('--num-channel-D', type=int, default = 64,
		help='Number of channels (?) for the Discriminator')
parser.add_argument('--num-channel-G', type=int, default = 64,
		help='Number of channels (?) for the Generator')
		
args, _ = parser.parse_known_args()

# Check cuda device
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Number of GPUs: ", torch.cuda.device_count())
    print("GPU name: ", torch.cuda.get_device_name(0))
else:
    device = torch.device("cpu")
    print("No GPU found !")

############################
#   Training information   #
############################

# Create a fixed glitch for sanity check: disabled by default
safety_glitch = False

# Create fixed noise to generate glitches if safety_glitch == True
fixed_noise = torch.randn((args.batch_size, 1, args.length_noise), device=device)


########################
#   Load the dataset   #
########################

print('Loading the data... Please wait...')

# Loading data
glitches_dataset = np.loadtxt(args.training_set)

# Initializing data loader
data_loader = torch.utils.data.DataLoader(glitches_dataset,
                                          batch_size= args.batch_size, shuffle=True)

num_batches = len(data_loader)

print('Data loaded succesfully !')
print("\tNumber of samples: {}\n".format(len(glitches_dataset)),
      "\tBatch size: {}\n".format(args.batch_size),
      "\tNumber of batches: {}".format(num_batches))

#############################
#   Initializing the nets   #
#############################

# Setting the D and G with random weigths
netG = model.GenerativeNet(args.num_channel_G)
netG.apply(model.init_weights)

netD = model.DiscriminativeNet(args.dropout_rate, args.num_channel_D, args.batch_size)
netD.apply(model.init_weights2)

netG = netG.to(device)
netD = netD.to(device)

#####################
#   Training loop   #
#####################

print("Starting Training Loop")

training_loop(args.ifo, args.n_epochs, data_loader, args.ncycles_D, 
			args.lambda_gp,
			args.lambda_ct, args.M_ct, args.batch_size,
			args.length_noise, device,
			netD, netG,
			args.lr_D, args.lr_G, fixed_noise,
			safety_glitch, args.out_dir)

print('End of training')




























